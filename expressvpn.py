import os
import sys
from functools import partial

from PySide6 import QtCore, QtGui, QtWidgets

from utils import (
    activate_command,
    check_connection,
    check_daemon,
    check_expressvpn,
    connect_command,
    disconnect_command,
    get_active_location,
    get_location_key,
    get_locations_list,
    get_protocol_list,
    get_preferences_dict,
    get_settings,
    is_activated,
    is_connected,
    set_network_lock,
    set_protocol,
    set_settings,
    RepeatingTimer,
)

DIR = os.path.dirname(os.path.abspath(__file__))
ICON = os.path.join(DIR, "assets/icon.png")
ICON_ACTIVE = os.path.join(DIR, "assets/icon_active.png")
LOGO = os.path.join(DIR, "assets/logo.png")
SETTINGS = os.path.join(DIR, "settings.dat")
TITLE = "ExpressVPN GUI"
UPDATE_INTERVAL = 2


class AppForm(QtWidgets.QMainWindow):
    def __init__(self, configure=False):
        super(AppForm, self).__init__()
        # Create System tray elements
        self.tray = QtWidgets.QSystemTrayIcon()
        self.tray_menu = QtWidgets.QMenu()
        self.tray_quit = QtGui.QAction("Quit", self)
        self.tray_status = QtGui.QAction("Disconnected", self)
        # Create UI elements
        self.connect_button = QtWidgets.QPushButton()
        self.layout = QtWidgets.QVBoxLayout()
        self.location_label = QtWidgets.QLabel()
        self.location_combo = QtWidgets.QComboBox()
        self.logo = QtGui.QPixmap(LOGO)
        self.network_lock_combo = QtWidgets.QComboBox()
        self.widget = QtWidgets.QWidget()
        self.logo_label = QtWidgets.QLabel(self.widget)
        self.options_group = QtWidgets.QGroupBox()
        self.protocol_label = QtWidgets.QLabel()
        self.protocol_combo = QtWidgets.QComboBox()
        self.thread = None
        self.update_timer = QtCore.QTimer()
        self.block_update_ui = False
        self.block_update_event = False
        self.updates = {}
        self.error_window = PopUpWindow()
        # Configure App
        if configure:
            self.configure()

    def configure(self):
        # System tray
        self.tray.setIcon(QtGui.QIcon(ICON))
        self.tray.setToolTip(TITLE)
        self.tray.setVisible(True)
        self.tray_quit.triggered.connect(self._close_event)
        self.tray_menu.addAction(self.tray_status)
        self.tray_menu.addSeparator()
        self.tray_menu.addAction(self.tray_quit)
        self.tray.setContextMenu(self.tray_menu)
        self.tray.activated.connect(self._focus_event)
        # Main UI
        self.setWindowTitle(TITLE)
        self.setFixedSize(QtCore.QSize(400, 500))
        self.setWindowIcon(QtGui.QIcon(ICON))
        self._create_horizontal_buttons()
        self.connect_button.setFixedHeight(48)
        self.network_lock_combo.setFixedHeight(32)
        self.network_lock_combo.addItems(["default", "strict", "off"])
        self.network_lock_combo.currentTextChanged.connect(self._network_lock_change)
        self.protocol_label.setText("Protocol and network lock:")
        self.protocol_label.setAlignment(QtCore.Qt.AlignCenter)
        self.protocol_combo.setFixedHeight(32)
        self.protocol_combo.addItems(get_protocol_list())
        self.protocol_combo.currentTextChanged.connect(self._protocol_change)
        self.location_label.setText("Select location:")
        self.location_label.setAlignment(QtCore.Qt.AlignCenter)
        self.location_combo.setFixedHeight(32)
        self.location_combo.addItems(get_locations_list())
        self.logo_label.setScaledContents(True)
        self.logo_label.setPixmap(self.logo)
        self.logo_label.setGeometry(QtCore.QRect(60, 40, 280, 200))
        self.layout.addWidget(self.protocol_label)
        self.layout.addWidget(self.options_group)
        self.layout.addWidget(self.location_label)
        self.layout.addWidget(self.location_combo)
        self.layout.addWidget(self.connect_button)
        self.layout.setContentsMargins(40, 260, 40, 40)
        self.widget.setLayout(self.layout)
        self.setCentralWidget(self.widget)
        last_location = get_settings(SETTINGS) or self.location_combo.currentText()
        self.location_combo.setCurrentIndex(
            self.location_combo.findText(last_location, QtCore.Qt.MatchFixedString)
        )
        # Update UI
        self._update_event()
        self._update_ui()
        self.thread = RepeatingTimer(UPDATE_INTERVAL, self._update_event)
        self.thread.start()
        self.update_timer.timeout.connect(self._update_ui)
        self.update_timer.setInterval(UPDATE_INTERVAL * 1000)
        self.update_timer.start()

    def closeEvent(self, event):
        event.ignore()
        self.hide()

    def _create_horizontal_buttons(self):
        layout = QtWidgets.QHBoxLayout()
        layout.addWidget(self.protocol_combo)
        layout.addWidget(self.network_lock_combo)
        layout.setContentsMargins(0, 0, 0, 0)
        self.options_group.setLayout(layout)
        self.options_group.setFlat(True)
        self.options_group.setObjectName("OptionsGroup")
        self.options_group.setStyleSheet("#OptionsGroup{border:0;}")

    def _network_lock_change(self):
        self.block_update_ui = True
        set_network_lock(self.network_lock_combo.currentText())
        self.block_update_ui = False
        self._update_event()

    def _protocol_change(self):
        self.block_update_ui = True
        set_protocol(self.protocol_combo.currentText())
        self.block_update_ui = False
        self._update_event()

    def _connect_vpn(self, force_location=False):
        if not force_location:
            location = self.location_combo.currentText()
        else:
            location = force_location
        self.connect_button.setText("Connecting...")
        self.connect_button.setDisabled(True)
        self.network_lock_combo.setDisabled(True)
        self.protocol_combo.setDisabled(True)
        self.location_combo.setDisabled(True)
        self.repaint()
        self.block_update_ui = True
        connect_command(get_location_key(location))

        while not self.updates["active_location"]:
            pass

        self.block_update_ui = False
        self._update_event()

    def _disconnect_vpn(self):
        self.connect_button.setText("Disconnecting...")
        self.connect_button.setDisabled(True)
        self.repaint()
        self.block_update_ui = True
        disconnect_command()

        while self.updates["active_location"]:
            pass

        self.block_update_ui = False
        self._update_event()

    def _update_ui(self):
        if self.block_update_ui:
            return True

        err_type = self.updates.get("error_type")
        if err_type:
            self.block_update_ui = True
            self.block_update_event = True
            self.get_error_window(err_type, update=True)
            self.error_window.show()

        try:
            self.connect_button.clicked.disconnect()
        except RuntimeError:
            pass
        try:
            self.tray_status.triggered.disconnect()
        except RuntimeError:
            pass

        active_location = self.updates.get("active_location")
        preferences = self.updates.get("preferences")

        if preferences:
            self.network_lock_combo.setCurrentIndex(
                self.network_lock_combo.findText(
                    preferences["network_lock"], QtCore.Qt.MatchFixedString
                )
            )
            self.protocol_combo.setCurrentIndex(
                self.protocol_combo.findText(
                    preferences["preferred_protocol"], QtCore.Qt.MatchFixedString
                )
            )
        self.connect_button.setDisabled(False)

        if not active_location:
            location = self.updates.get("location")
            self.connect_button.setText("Connect")
            self.connect_button.clicked.connect(self._connect_vpn)
            self.network_lock_combo.setDisabled(False)
            self.protocol_combo.setDisabled(False)
            self.location_combo.setDisabled(False)
            self.tray_status.setText(f"Reconnect - {location}")
            connect_vpn = partial(self._connect_vpn, force_location=location)
            self.tray_status.triggered.connect(connect_vpn)
            self.tray.setIcon(QtGui.QIcon(ICON))
        else:
            self.connect_button.setText("Disconnect")
            self.connect_button.clicked.connect(self._disconnect_vpn)
            self.location_combo.setCurrentIndex(
                self.location_combo.findText(
                    active_location, QtCore.Qt.MatchFixedString
                )
            )
            self.network_lock_combo.setDisabled(True)
            self.protocol_combo.setDisabled(True)
            self.location_combo.setDisabled(True)
            self.tray_status.setText(f"Disconnect: {self.location_combo.currentText()}")
            self.tray_status.triggered.connect(self._disconnect_vpn)
            self.tray.setIcon(QtGui.QIcon(ICON_ACTIVE))
            set_settings(SETTINGS, self.location_combo.currentText())

        self.repaint()

    @staticmethod
    def check_errors(update=False):
        if not update and not check_connection():
            return "internet_connection_error"
        if not check_expressvpn():
            return "expressvpn_error"
        if not check_daemon():
            return "expressvpn_daemon_error"
        if not is_activated():
            return "expressvpn_activation_error"

    def get_error_window(self, error, update=False):
        if error == "internet_connection_error":
            self.error_window.action = "quit"
            self.error_window.message_box("Please check your internet connection")
            return

        if error == "expressvpn_error":
            self.error_window.action = "quit"
            self.error_window.message_box(
                "Please install expressvpn in order to use GUI"
            )
            return

        if error == "expressvpn_daemon_error":
            self.error_window.action = "quit"
            self.error_window.message_box(
                "Please make sure that expressvpn daemon is running"
            )
            return

        if error == "expressvpn_activation_error":
            if not update:
                self.error_window.action = "quit"
                self.error_window.main_form = self
                self.error_window.activation_box()
            else:
                self.error_window.action = "quit"
                self.error_window.message_box(
                    "Please restart the GUI in order to activate expressvpn"
                )
            return

    def _update_event(self):
        if self.block_update_event:
            return

        err_type = self.check_errors(update=True)
        if err_type:
            self.updates["error_type"] = err_type
            self.thread.cancel()
            return

        active_location = get_active_location()
        preferences = get_preferences_dict()
        location = get_settings(SETTINGS) or self.location_combo.currentText()
        self.updates = {
            "active_location": active_location,
            "preferences": preferences,
            "location": location,
        }

    def _close_event(self):
        self.block_update_event = True
        self.thread.cancel()
        if is_connected():
            disconnect_command()
        exit()

    def _focus_event(self):
        self.show()
        self.setFocus()
        self.activateWindow()
        self.raise_()


class PopUpWindow(QtWidgets.QDialog):
    def __init__(self, main_form=None, action="close"):
        super(PopUpWindow, self).__init__()
        self.action = action
        self.main_form = main_form
        self._configure()

    def _configure(self):
        self.setWindowTitle(TITLE)
        self.setWindowIcon(QtGui.QIcon(ICON))
        self.setFixedWidth(400)

    def message_box(self, text):
        layout = QtWidgets.QVBoxLayout()
        button_layout = QtWidgets.QHBoxLayout()
        message_text = QtWidgets.QLabel()
        message_text.setAlignment(QtCore.Qt.AlignCenter)
        message_text.setText(text)
        ok_button = QtWidgets.QPushButton()
        ok_button.setText("OK")
        ok_button.clicked.connect(self.closeEvent)
        button_group = QtWidgets.QGroupBox()
        button_group.setObjectName("PopupButtons")
        button_group.setStyleSheet("#PopupButtons{border:0;}")
        button_layout.addWidget(ok_button)
        button_layout.setContentsMargins(80, 0, 80, 0)
        button_group.setLayout(button_layout)
        button_group.setFlat(True)
        layout.setAlignment(QtCore.Qt.AlignCenter)
        layout.setSpacing(30)
        layout.addWidget(message_text)
        layout.addWidget(button_group)
        layout.setContentsMargins(10, 20, 10, 20)
        self.setLayout(layout)
        self.setFixedHeight(150)

    def activation_box(self):
        def on_ok():
            code = activation_code.text() or "N/A"
            activate_command(code)
            if not is_activated():
                activation_popup = PopUpWindow(action="close")
                activation_popup.message_box("Invalid activation code!")
                activation_popup.show()
                if activation_popup.exec_() == QtWidgets.QDialog.Accepted:
                    return
            else:
                self.main_form.configure()
                self.hide()

        layout = QtWidgets.QVBoxLayout()
        button_layout = QtWidgets.QHBoxLayout()
        message_text = QtWidgets.QLabel()
        message_text.setAlignment(QtCore.Qt.AlignCenter)
        message_text.setText("Insert your activation code:")
        activation_code = QtWidgets.QLineEdit()
        activation_code.setEchoMode(QtWidgets.QLineEdit.Password)
        ok_button = QtWidgets.QPushButton()
        ok_button.setText("OK")
        ok_button.clicked.connect(on_ok)
        cancel_button = QtWidgets.QPushButton()
        cancel_button.setText("Cancel")
        cancel_button.clicked.connect(self.closeEvent)
        button_group = QtWidgets.QGroupBox()
        button_group.setObjectName("PopupButtons")
        button_group.setStyleSheet("#PopupButtons{border:0;}")
        button_layout.addWidget(ok_button)
        button_layout.addWidget(cancel_button)
        button_layout.setContentsMargins(80, 0, 80, 0)
        button_group.setLayout(button_layout)
        button_group.setFlat(True)
        layout.setAlignment(QtCore.Qt.AlignCenter)
        layout.setSpacing(10)
        layout.addWidget(message_text)
        layout.addWidget(activation_code)
        layout.addWidget(button_group)
        layout.setContentsMargins(10, 20, 10, 20)
        self.setLayout(layout)
        self.setFixedHeight(150)

    def closeEvent(self, event):
        if event:
            event.ignore()
        if self.action == "close":
            self.hide()
        elif self.action == "quit":
            if is_connected():
                disconnect_command()
            exit()


if __name__ == "__main__":
    # Create the Qt Application
    app = QtWidgets.QApplication(sys.argv)
    app.setDesktopFileName("expressvpn-gui")
    form = AppForm()
    error_type = form.check_errors()

    if error_type:
        form.get_error_window(error_type)
        form.error_window.show()
    else:
        form.configure()

    # Run the main Qt loop
    sys.exit(app.exec())
