# ExpressVPN GUI
Unofficial linux GUI for expressvpn CLI created with Python (PySide6)

### Requirements
- expressvpn
- PySide6
- python-pexpect

### Installation
- Clone the repo inside your /opt directory
- Copy expressvpn-gui to /usr/bin directory
- Copy expressvpn-gui.desktop to /usr/share/application directory

### License
Original [ExpressVPN License](https://www.expressvpn.com/vpn-software/vpn-linux/open-source) applies
